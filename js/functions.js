$(document).ready(function() {
  $("button#show-holidays").on("click", function() {
    var url = "http://nolaborables.com.ar/API/v1/actual";
    $.ajax(url, {
      success: function(data) {
        data.forEach(function(element) {
    	
          $('#holidays ul').append("<li> Dia: " + element.dia + ", mes: " + element.mes + ", motivo: " + element.motivo + "</li>");
        }, this);
      },
      error: function(data) {
        alert('Algo salió mal');
      }
    })
  });
});